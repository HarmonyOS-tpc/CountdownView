﻿# CountDownView

CountDownView是一个具有倒计时功能的View,满足大多数倒计时控件需求

## 主要特征
 * 支持修改倒计时背景颜色
 * 支持修改倒计时数字颜色
 * 支持倒计时分割符自定义
 * 支持倒计时边框圆角修改
 
 
## 演示

|无背景倒计时 | 有背景倒计时|列表倒计时使用 |
|:---:|:---:|:---:|
|<img src="screenshot/nobackground.gif" width="75%"/>|<img src="screenshot/hasbackground.gif" width="75%"/>|<img src="screenshot/list.gif" width="75%"/>|

图片过大需要登陆后可见

## entry运行要求
 通过DevEco studio,并下载openHarmonySDK
 将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## 集成

```
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:countdownview:1.0.1'
```

CountdownView使用说明：
### 代码使用
```
CountdownView mCvCountdownView = (CountdownView)findViewById(R.id.cv_countdownViewTest1);
mCvCountdownView.start(995550000); // Millisecond
```
### 布局使用
```xml
<cn.iwgang.example.CountdownView
        ohos:id="$+id:cv_countdownViewTest1"
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:layout_alignment="horizontal_center"
        ohos:top_margin="20vp"
        ohos:isHideTimeBackground="false"
        ohos:isShowDay="false"
        ohos:isShowHour="true"
        ohos:isShowMillisecond="false"
        ohos:isShowMinute="true"
        ohos:isShowSecond="true"
        ohos:isShowTimeBgDivisionLine="false"
        ohos:suffixGravity="center"
        ohos:suffixTextColor="#000000"
        ohos:suffixTextSize="20vp"
        ohos:timeBgColor="#FF5000"
        ohos:timeTextColor="#FFFFFF"
        ohos:timeTextSize="20vp"/>
```

### 自定义属性
| attr | type | default|
| --------   | :-----:  | :----: |
|isHideTimeBackground | boolean | true|
|timeBgColor  | color      | #444444|
|timeBgSize   | dimension  | timeSize + 2vp * 4|
|timeBgRadius | dimension  | 0|
|isShowTimeBgDivisionLine | boolean  | true|
|timeBgDivisionLineColor | color | #30FFFFFF|
|timeBgDivisionLineSize  | dimension | 0.5vp|
|timeTextSize   | dimension | 12fp |
|timeTextColor  | color | #000000|
|isTimeTextBold | boolean | false|
|isShowDay  | boolean | auto show (has value show if not hide)|
|isShowHour  | boolean | auto show (has value show if not hide)|
|isShowMinute  | boolean | true|
|isShowSecond  | boolean | true|
|isShowMillisecond  | boolean | false|
|isConvertDaysToHours | boolean | false|
|suffixTextSize | dimension | 12fp|
|suffixTextColor  | color | #000000|
|isSuffixTextBold  | boolean | false|
|suffixGravity | 'top' or 'center' or 'bottom' | 'center'|
|suffix | string | ':'|
|suffixDay  | string | null|
|suffixHour  | string | null|
|suffixMinute  | string | null|
|suffixSecond  | string | null|
|suffixMillisecond  | string | null|
|suffixLRMargin  | dimension | left 3vp right 3vp|
|suffixDayLeftMargin | dimension | 0|
|suffixDayRightMargin  | dimension | 0|
|suffixHourLeftMargin  | dimension | 0|
|suffixHourRightMargin  | dimension | 0|
|suffixMinuteLeftMargin | dimension | 0|
|suffixMinuteRightMargin  | dimension | 0|
|suffixSecondLeftMargin  | dimension | 0|
|suffixSecondRightMargin  | dimension | 0|
|suffixMillisecondLeftMargin | dimension | 0|
|isShowTimeBgBorder | boolean | false|
|timeBgBorderColor  | color | #000000|
|timeBgBorderSize  | dimension | 1vp|
|timeBgBorderRadius  | dimension | 0|

## 注意待优化功能：
配合listcontainer使用时，countdownview的宽高要给精确值，
不能使用match_content,否则列表条目宽高展示会有问题



