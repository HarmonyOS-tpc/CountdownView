/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.iwgang.example.slice;

import cn.iwgang.countdownview.CountdownView;


import cn.iwgang.example.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 程序显示主界面入口
 */
public class MainAbilitySlice extends AbilitySlice {

    private static final int MSG_TIME_UP = 12;
    long time;
    private EventHandler eventHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        CountdownView mCvCountdownViewTest1 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest1);
        mCvCountdownViewTest1.setTag("test1");
        long time1 = (long)5 * 60 * 60 * 1000;
        mCvCountdownViewTest1.start(time1);

        CountdownView mCvCountdownViewTest2 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest2);
        mCvCountdownViewTest2.setTag("test2");
        long time2 = (long)30 * 60 * 1000;
        mCvCountdownViewTest2.start(time2);

        CountdownView cvCountdownViewTest211 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest211);
        cvCountdownViewTest211.setTag("test21");
        long time211 = (long)30 * 60 * 1000;
        cvCountdownViewTest211.start(time211);

        CountdownView mCvCountdownViewTest21 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest21);
        mCvCountdownViewTest21.setTag("test21");
        long time21 = (long)24 * 60 * 60 * 1000;
        mCvCountdownViewTest21.start(time21);

        CountdownView mCvCountdownViewTest22 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest22);
        mCvCountdownViewTest22.setTag("test22");
        long time22 = (long)30 * 60 * 1000;
        mCvCountdownViewTest22.start(time22);

        CountdownView mCvCountdownViewTest3 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest3);
        long time3 = (long)9 * 60 * 60 * 1000;
        mCvCountdownViewTest3.start(time3);

        CountdownView mCvCountdownViewTest4 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest4);
        long time4 = (long)150 * 24 * 60 * 60 * 1000;
        mCvCountdownViewTest4.start(time4);

        CountdownView cvConvertDaysToHours = (CountdownView) findComponentById(ResourceTable.Id_cv_convertDaysToHours);

        cvConvertDaysToHours.start(time4);

        final CountdownView mCvCountdownViewTest5 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest5);


        eventHandler = new EventHandler(EventRunner.getMainEventRunner()){
            @Override
            protected void processEvent(InnerEvent event) {
                super.processEvent(event);
                mCvCountdownViewTest5.updateShow(time);
            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                time += 1000;
                eventHandler.sendEvent(MSG_TIME_UP);

            }
        },1000,1000);


        CountdownView mCvCountdownViewTest6 = (CountdownView)findComponentById(ResourceTable.Id_cv_countdownViewTest6);
        long time6 = (long)2 * 60 * 60 * 1000;
        mCvCountdownViewTest6.start(time6);

        initClickListener();
    }

    private void initClickListener() {
        Button button1 = (Button) findComponentById(ResourceTable.Id_btn_toDynamicShowAbility);
        Button button2 = (Button) findComponentById(ResourceTable.Id_btn_toListViewAbility);
        button1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("cn.iwgang.example")
                        .withAbilityName("cn.iwgang.example.DynamicShowAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);

            }
        });
        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("cn.iwgang.example")
                        .withAbilityName("cn.iwgang.example.ListContainerAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {

        super.onForeground(intent);
    }
}
